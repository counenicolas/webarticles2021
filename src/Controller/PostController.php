<?php

namespace App\Controller;

use App\Entity\Post;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends AbstractController
{
    #[Route('/old', name: 'old_posts')]
    /**
     * Affiche tous les articles
     * @return Response
     */

    // Méthode complète: récupérer les services Doctrine, invoquer le Repository (nom de l'entité) et appeler la méthode magique de sélection
    public function OldPosts(): Response
    {
        $posts = $this->getDoctrine()
                      ->getRepository(Post::class)
                      ->findBy(
                          ['isPublished' => true],
                          ['title' => 'ASC']
                      );
        //dump($posts);
        return $this->render('post/posts.html.twig', ['posts' => $posts]);
    }

    #[Route('/', name: 'posts')]
    /**
     * @param PostRepository $repository
     * @return Response
     */

    // Méthode optimisée en utilisant l'injection de dépendance.  Dans la méthode on injecte le Repository et sur l'objet ($repository) on invoque la méthode magique
    public function posts(PostRepository $repository) :Response
    {
        $posts = $repository->findBy(
            ['isPublished' => true],
            ['title' => 'ASC']
        );
        return $this->render('post/posts.html.twig', ['posts' => $posts]);
    }

    #[Route('/postold/{id}', name: 'post_old')]
    /**
     * @param PostRepository $repository
     * @param int $id
     * @return Response
     */
    public function postOld(PostRepository $repository, int $id): Response
    {
        $post = $repository->find($id);
        //dd($post);
        return $this->render('post/detail.html.twig', ['post' => $post]);
    }

    #[Route('/post/{id}', name: 'post')]
    /**
     * @param Post $post
     * @return Response
     */
    // PARAMCONVERTER
    public function post(Post $post): Response
    {
        return $this->render('post/detail.html.twig', ['post' => $post]);
    }

    #[Route('/delpost/{id}', name: 'delpost')]
    /**
     * @param Post $post
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delPost(Post $post, EntityManagerInterface $manager): Response
    {
        $manager->remove($post);
        $manager->flush();
        return $this->redirectToRoute('posts');
    }

}
